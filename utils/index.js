const customHandlers = require('./customHandlers');
const transformData = require('./transformData');
const customHelpers = require('./customHelpers');

module.exports = {
  ...customHandlers,
  ...transformData,
  ...customHelpers,
};
