const nodemailer =  require('nodemailer');

// interface MailOptions {
//   to: string;
//   subject: string;
//   text?: string;
//   html?: string;
// }

module.exports = class MailerService {
  transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.MAILER_LOGIN,
      pass: process.env.MAILER_PASSWORD,
    },
  });

  send = async ops => {
    const mailOptions = {
      from: '"Magisoft" <mgs.hr.manager@gmail.com>',
      ...ops,
    };

    return this.transporter.sendMail(mailOptions);
  };
}