exports.transformTranslationData = items => {
  const getRes = (lang, arr) =>
    arr.reduce((acc, item) => {
      acc[item.key] = item[lang];
      return acc;
    }, {});

  const response = [
    {
      lang: 'en',
      translation: getRes('en', items),
    },
    {
      lang: 'ru',
      translation: getRes('ru', items),
    },
    {
      lang: 'ua',
      translation: getRes('ua', items),
    },
  ];

  return response;
};

exports.generationObjTranslation = (en, ru, ua) => {
  const obj = {};

  if (en) obj.en = en;
  if (ru) obj.ru = ru;
  if (ua) obj.ua = ua;

  return obj;
};
