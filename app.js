const express = require('express');
const passport = require('passport');
const swaggerUi = require('swagger-ui-express');
const cors = require('cors');
const logger = require('morgan')('dev');
const path = require('path');

const middleware = require('./middleware');
const swaggerDocument = require('./swagger.json');

const authRouters = require('./routers/authRouter');
const vacancyRouters = require('./routers/vacancyRouter');
const internalizationRouters = require('./routers/internalizationRouter');
const clientContactsRouter = require('./routers/clientContactsRouter');
const portfolioRouter = require('./routers/portfolioRouter');
const technologyRouter = require('./routers/technologyRouter');
const jobApplicationRouter = require('./routers/jobApplicationRouter');
const servicesRouter = require('./routers/servicesRouter');
const teamRouter = require('./routers/teamRouter');

const app = express();

app.use(cors());
app.use('/assets', express.static(path.join(__dirname, 'uploads')));

// Passport Middlewares
middleware.passportJwt(passport);
middleware.passportLocal(passport);

// Added logging
if (process.env.NODE_ENV === 'development') {
  app.use(logger);
}

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// // Init Swagger. Access: /api-docs
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Passport JS is what we use to handle our logins
app.use(passport.initialize());
app.use(passport.session());

app.use('/api/auth', authRouters);
app.use('/api/vacancy', vacancyRouters);
app.use('/api/internalization', internalizationRouters);
app.use('/api/clientContacts', clientContactsRouter);
app.use('/api/portfolio', portfolioRouter);
app.use('/api/technology', technologyRouter);
app.use('/api/jobApplication', jobApplicationRouter);
app.use('/api/service', servicesRouter);
app.use('/api/team', teamRouter);

// If that above routes didn't work, we get 404 and forward to error handler
app.use(middleware.routeNotFound);

app.use(middleware.dbValidationErrors);

if (process.env.NODE_ENV === 'development') {
  /* Development Error Handler - Prints stack trace */
  app.use(middleware.developmentErrors);
}

// production error handler
app.use(middleware.productionErrors);

module.exports = app;
