const path = require('path');
const fs = require('fs');

const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const Technology = require('../models/Technology');
const Internalization = require('../models/Internalization');

const request = supertest(app);

const helpers = require('./helpers');
const dataMock = require('./mock/technology');

const { pathRootDir } = require('../middleware/multer');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
  await dataHandler.deleteWorkDir();
});

describe('Team router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/technology');

      expect(res.statusCode).toBe(200);
      expect(res.body).toEqual([]);
    });

    test('Should get an array with some data', async () => {
      const technology = new Technology(dataMock.dbData);
      await technology.save();

      const res = await request.get('/api/technology');

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveLength(1);
      expect(res.body[0]).toHaveProperty('title', dataMock.dbData.title);
      expect(res.body[0]).toHaveProperty('summary', dataMock.dbData.summary);
      expect(res.body[0]).toHaveProperty('description', dataMock.dbData.description);
      expect(res.body[0]).toHaveProperty('logo', dataMock.dbData.logo);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/technology')
        .set('Content-Type', 'multipart/form-data')
        .field('titleEn', dataMock.fullData.titleEn)
        .field('titleRu', dataMock.fullData.titleRu)
        .field('titleUa', dataMock.fullData.titleUa)
        .field('summaryEn', dataMock.fullData.summaryEn)
        .field('summaryRu', dataMock.fullData.summaryRu)
        .field('summaryUa', dataMock.fullData.summaryUa)
        .field('descEn', dataMock.fullData.descEn)
        .field('descRu', dataMock.fullData.descRu)
        .field('descUa', dataMock.fullData.descUa)
        .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty('title');
      expect(res.body).toHaveProperty('summary');
      expect(res.body).toHaveProperty('description');
      expect(res.body).toHaveProperty('logo');

      const traslationTitle = await Internalization.findOne({ key: res.body.title });
      expect(traslationTitle).toHaveProperty('en', dataMock.fullData.titleEn);
      expect(traslationTitle).toHaveProperty('ru', dataMock.fullData.titleRu);
      expect(traslationTitle).toHaveProperty('ua', dataMock.fullData.titleUa);

      const traslationSumr = await Internalization.findOne({ key: res.body.summary });
      expect(traslationSumr).toHaveProperty('en', dataMock.fullData.summaryEn);
      expect(traslationSumr).toHaveProperty('ru', dataMock.fullData.summaryRu);
      expect(traslationSumr).toHaveProperty('ua', dataMock.fullData.summaryUa);

      const traslationDesc = await Internalization.findOne({ key: res.body.description });
      expect(traslationDesc).toHaveProperty('en', dataMock.fullData.descEn);
      expect(traslationDesc).toHaveProperty('ru', dataMock.fullData.descRu);
      expect(traslationDesc).toHaveProperty('ua', dataMock.fullData.descUa);

      expect(fs.existsSync(path.join(pathRootDir, res.body.logo))).toBe(true);
    });
  });

  describe('PUT', () => {
    test('Should update the entry in the database', async () => {
      const traslationTitle = new Internalization({
        key: dataMock.dbData.title,
        en: dataMock.fullData.titleEn,
        ru: dataMock.fullData.titleRu,
        ua: dataMock.fullData.titleUa,
      });
      await traslationTitle.save();

      const traslationSumr = new Internalization({
        key: dataMock.dbData.summary,
        en: dataMock.fullData.summaryEn,
        ru: dataMock.fullData.summaryRu,
        ua: dataMock.fullData.summaryUa,
      });
      await traslationSumr.save();

      const traslationDesc = new Internalization({
        key: dataMock.dbData.description,
        en: dataMock.fullData.descEn,
        ru: dataMock.fullData.descRu,
        ua: dataMock.fullData.descUa,
      });
      await traslationDesc.save();

      await helpers.copyFile('./mock/mockPng.png', dataMock.dbData.logo);

      const technology = new Technology(dataMock.dbData);
      await technology.save();

      const res = await request
        .put(`/api/technology/${technology._id}`)
        .set('Content-Type', 'multipart/form-data')
        .field('titleEn', dataMock.dataWithoutSomeFilds.titleEn)
        .field('summaryRu', dataMock.dataWithoutSomeFilds.summaryRu)
        .field('descUa', dataMock.dataWithoutSomeFilds.descUa)
        .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('title');
      expect(res.body).toHaveProperty('summary');
      expect(res.body).toHaveProperty('description');
      expect(res.body).toHaveProperty('logo');

      const traslationTitleAfter = await Internalization.findOne({ key: res.body.title });
      expect(traslationTitleAfter).toHaveProperty('en', dataMock.dataWithoutSomeFilds.titleEn);

      const traslationSumrAfter = await Internalization.findOne({ key: res.body.summary });
      expect(traslationSumrAfter).toHaveProperty('ru', dataMock.dataWithoutSomeFilds.summaryRu);

      const traslationDescAfter = await Internalization.findOne({ key: res.body.description });
      expect(traslationDescAfter).toHaveProperty('ua', dataMock.dataWithoutSomeFilds.descUa);

      expect(fs.existsSync(path.join(pathRootDir, res.body.logo))).toBe(true);
    });

    test('Should generate an error when updating an entry without an id', async () => {
      const res = await request
        .put(`/api/technology/`)
        .set('Content-Type', 'multipart/form-data')
        .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const traslationTitle = new Internalization({
        key: dataMock.dbData.title,
        en: dataMock.fullData.titleEn,
        ru: dataMock.fullData.titleRu,
        ua: dataMock.fullData.titleUa,
      });
      await traslationTitle.save();

      const traslationSumr = new Internalization({
        key: dataMock.dbData.summary,
        en: dataMock.fullData.summaryEn,
        ru: dataMock.fullData.summaryRu,
        ua: dataMock.fullData.summaryUa,
      });
      await traslationSumr.save();

      const traslationDesc = new Internalization({
        key: dataMock.dbData.description,
        en: dataMock.fullData.descEn,
        ru: dataMock.fullData.descRu,
        ua: dataMock.fullData.descUa,
      });
      await traslationDesc.save();

      const technology = new Technology({ ...dataMock.dbData, logo: dataMock.dbDataLogoForDelete });
      await technology.save();

      await helpers.copyFile('./mock/mockPng.png', dataMock.dbDataLogoForDelete);

      const res = await request.delete(`/api/technology/${technology._id}`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('message');

      const traslationTitleAfter = await Internalization.findOne({ key: res.body.title });
      const traslationSumrAfter = await Internalization.findOne({ key: res.body.summary });
      const traslationDescAfter = await Internalization.findOne({ key: res.body.description });
      const technologyAfter = await Technology.findById(technology._id);

      expect(!!traslationTitleAfter).toBe(false);
      expect(!!traslationSumrAfter).toBe(false);
      expect(!!traslationDescAfter).toBe(false);
      expect(!!technologyAfter).toBe(false);

      expect(fs.existsSync(path.join(pathRootDir, dataMock.dbDataLogoForDelete))).toBe(false);
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request.put(`/api/technology/`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
