exports.fullContacts = {
  email: 'test@email.com',
  message: 'Some message',
  fullname: 'Lastname Firstname',
  skype: 'Skype',
  companyName: 'Some company',
  projectType: 'Web site',
};

exports.shortContacts = {
  email: 'test@email.com',
  message: 'Some message',
  fullname: 'Lastname Firstname',
};

exports.moreShortContacts = {
  email: 'test@email.com',
  message: 'Some message',
};

exports.contactsWithoutSomeRequiredFields = {
  fullname: 'Lastname Firstname',
  message: 'Some message',
};
