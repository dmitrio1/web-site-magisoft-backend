exports.fullData = {
  descEn: 'Some desc eng',
  descRu: 'Some desc rus',
  descUa: 'Some desc uah',
  logo: 'portfolio_logo_file_name.png',
  slide: 'portfolio_slide_file_name.png',
};

exports.dataWitoutSomeFilds = {
  descEn: 'Some desc eng [?some data]',
  descUa: 'Some desc uah [?some data]',
  logo: 'portfolio_logo_file_name.png',
};

exports.dbData = {
  description: 'some_key_for_internalization',
  logo: 'portfolio_logo_file_name.png',
  slide: 'portfolio_slide_file_name.png',
};
