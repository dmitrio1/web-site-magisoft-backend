exports.fullData = {
  nameEn: 'nameEn',
  nameRu: 'nameRu',
  nameUa: 'nameUa',
  positionEn: 'positionEn',
  positionRu: 'positionRu',
  positionUa: 'positionUa',
  location: '2',
};

exports.dataWithoutSomeFilds = {
  nameEn: 'new nameEn',
  positionUa: 'new positionUa',
  location: '1',
};

exports.dbData = {
  name: 'some_key_for_intern_name',
  position: 'some_key_for_intern_position',
  image: 'some_employee_img_path.png',
  location: '3',
};
