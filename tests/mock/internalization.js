exports.fullTranslation = {
  key: 'some_key_hsdfsdfds',
  en: 'some eng text',
  ru: 'some rus text',
  ua: 'some uah text',
};

exports.translationWithoutSomeFields = {
  key: 'some_key_hsdfsdfds',
  en: 'some eng text [?some text]',
  ua: 'some uah text [?some text]',
};

exports.translationWithoutKey = {
  en: 'some eng text [?some text]',
  ru: 'some rus text [?some text]',
  ua: 'some uah text [?some text]',
};

exports.resEmptyTranslationData = [
  {
    lang: 'en',
    translation: {},
  },
  {
    lang: 'ru',
    translation: {},
  },
  {
    lang: 'ua',
    translation: {},
  },
];

exports.resTranslationData = [
  {
    lang: 'en',
    translation: {
      some_key_hsdfsdfds: 'some eng text',
    },
  },
  {
    lang: 'ru',
    translation: {
      some_key_hsdfsdfds: 'some rus text',
    },
  },
  {
    lang: 'ua',
    translation: {
      some_key_hsdfsdfds: 'some uah text',
    },
  },
];
