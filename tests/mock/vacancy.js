exports.fullData = {
  name: 'Some vacancy',
  requirementsEn: 'requirementsEn',
  requirementsRu: 'requirementsRu',
  requirementsUa: 'requirementsUa',
  willBePlusEn: 'willBePlusEn',
  willBePlusRu: 'willBePlusRu',
  willBePlusUa: 'willBePlusUa',
  responsibilitiesEn: 'responsibilitiesEn',
  responsibilitiesRu: 'responsibilitiesRu',
  responsibilitiesUa: 'responsibilitiesUa',
  conditionsEn: 'conditionsEn',
  conditionsRu: 'conditionsRu',
  conditionsUa: 'conditionsUa',
  isEnabled: true,
  specialization: 'frontend',
};

exports.fullDataWithoutSpecialization = {
  name: 'Some vacancy',
  requirementsEn: 'requirementsEn',
  requirementsRu: 'requirementsRu',
  requirementsUa: 'requirementsUa',
  willBePlusEn: 'willBePlusEn',
  willBePlusRu: 'willBePlusRu',
  willBePlusUa: 'willBePlusUa',
  responsibilitiesEn: 'responsibilitiesEn',
  responsibilitiesRu: 'responsibilitiesRu',
  responsibilitiesUa: 'responsibilitiesUa',
  conditionsEn: 'conditionsEn',
  conditionsRu: 'conditionsRu',
  conditionsUa: 'conditionsUa',
  isEnabled: true,
};

exports.dataWithoutSomeFields = {
  name: 'Some vacancy name',
  requirementsEn: 'requirementsEn second',
  willBePlusUa: 'willBePlusUa second',
  responsibilitiesEn: 'responsibilitiesEn second',
  conditionsRu: 'conditionsRu second',
  isEnabled: false,
};

exports.dbData = {
  name: 'Some name',
  requirements: 'key_for_int_vacancy_requirements',
  willBePlus: 'key_for_int_vacancy_willbeplus',
  responsibilities: 'key_for_int_vacancy_responsibilities',
  conditions: 'key_for_int_vacancy_conditions',
  isEnabled: true,
  specialization: 'frontend',
};
