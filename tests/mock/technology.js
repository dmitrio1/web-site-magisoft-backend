exports.fullData = {
  titleEn: 'te',
  titleRu: 'tr',
  titleUa: 'tu',
  summaryEn: 'se',
  summaryRu: 'sr',
  summaryUa: 'su',
  descEn: 'de',
  descRu: 'dr',
  descUa: 'du',
  logo: 'some_logo_for_technology.png',
};

exports.dataWithoutSomeFilds = {
  titleEn: 'te update data',
  titleRu: '',
  titleUa: '',
  summaryEn: '',
  summaryRu: 'sr update data',
  summaryUa: '',
  descEn: '',
  descRu: '',
  descUa: 'du update data',
  logo: 'some_logo_for_technology.png',
};

exports.dbData = {
  title: 'some_key_for_int_name_technology',
  summary: 'some_key_for_int_summary_technology',
  description: 'some_key_for_int_desc_technology',
  logo: 'some_logo_for_technology.png',
};

exports.dbDataLogoForDelete = 'some_logo_for_delete_technology.png';
