exports.fullService = {
  titleEn: 'Eng title',
  titleRu: 'Rus title',
  titleUa: 'Uah title',
  descEn: 'Eng desc',
  descRu: 'Rus desc',
  descUa: 'Uah desc',
};

exports.serviceWithoutSomeFields = {
  titleEn: 'Eng title [?long text]',
  descUa: 'Uah desc [?some text]',
};

exports.dbData = {
  title: 'some_key_for_int_title',
  description: 'some_key_for_int_desc',
};
