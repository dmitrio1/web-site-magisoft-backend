const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const ClientContacts = require('../models/ClientContacts');
const contactMock = require('./mock/clientContact');

const helpers = require('./helpers');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
});

describe('ClientContact router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/clientContacts').set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveLength(0);
    });

    test('Should get an array with some data', async () => {
      const contact = new ClientContacts(contactMock.shortContacts);
      await contact.save();

      const res = await request.get('/api/clientContacts').set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveLength(1);
      helpers.expectResDataToEqual(res.body[0], contactMock.shortContacts);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/clientContacts')
        .send(contactMock.shortContacts)
        .set('Accept', 'application/json');

      expect(res.statusCode).toEqual(201);
      expect(res.body).toHaveProperty('message');

      const contact = await ClientContacts.findOne({ email: contactMock.shortContacts.email });
      helpers.expectDBDataToEqual(contact, contactMock.shortContacts);
    });

    test('Should generate an error when some necessary data does not exist', async () => {
      const res = await request
        .post('/api/clientContacts')
        .send(contactMock.contactsWithoutSomeRequiredFields)
        .set('Accept', 'application/json');

      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const contact = new ClientContacts(contactMock.fullContacts);
      await contact.save();

      const res = await request
        .delete(`/api/clientContacts/${contact._id}`)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('message');

      const contacts = await ClientContacts.find({ email: contactMock.fullContacts.email });
      expect(contacts).toHaveLength(0);
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request
        .delete(`/api/clientContacts/5e42bf0c87099a741e53b23f`)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
