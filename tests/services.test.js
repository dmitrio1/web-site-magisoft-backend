const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const dataMock = require('./mock/services');
const Services = require('../models/Services');
const Internalization = require('../models/Internalization');

const helpers = require('./helpers');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
});

describe('Services router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/service');

      expect(res.statusCode).toBe(200);
      expect(res.body).toEqual([]);
    });

    test('Should get an array with some data', async () => {
      const service = new Services({ ...dataMock.dbData });
      await service.save();

      const res = await request.get('/api/service');

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveLength(1);
      expect(res.body[0]).toHaveProperty('title', dataMock.dbData.title);
      expect(res.body[0]).toHaveProperty('description', dataMock.dbData.description);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/service')
        .send(dataMock.fullService)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty('description');
      expect(res.body).toHaveProperty('title');

      const traslationDesc = await Internalization.findOne({ key: res.body.description });
      expect(traslationDesc).toHaveProperty('en', dataMock.fullService.descEn);
      expect(traslationDesc).toHaveProperty('ru', dataMock.fullService.descRu);
      expect(traslationDesc).toHaveProperty('ua', dataMock.fullService.descUa);

      const traslationTitle = await Internalization.findOne({ key: res.body.title });
      expect(traslationTitle).toHaveProperty('en', dataMock.fullService.titleEn);
      expect(traslationTitle).toHaveProperty('ru', dataMock.fullService.titleRu);
      expect(traslationTitle).toHaveProperty('ua', dataMock.fullService.titleUa);
    });
  });

  describe('PUT', () => {
    test('Should update the entry in the database', async () => {
      const traslationTitle = new Internalization({
        key: dataMock.dbData.title,
        en: dataMock.fullService.titleEn,
        ru: dataMock.fullService.titleRu,
        ua: dataMock.fullService.titleUa,
      });
      await traslationTitle.save();

      const traslationDecs = new Internalization({
        key: dataMock.dbData.description,
        en: dataMock.fullService.descEn,
        ru: dataMock.fullService.descRu,
        ua: dataMock.fullService.descUa,
      });
      await traslationDecs.save();

      const service = new Services(dataMock.dbData);
      await service.save();

      const res = await request
        .put(`/api/service/${service._id}`)
        .send(dataMock.serviceWithoutSomeFields)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('description');
      expect(res.body).toHaveProperty('title');

      const traslationDescRead = await Internalization.findOne({ key: res.body.description });
      expect(traslationDescRead).toHaveProperty('ua', dataMock.serviceWithoutSomeFields.descUa);

      const traslationTitleRead = await Internalization.findOne({ key: res.body.title });
      expect(traslationTitleRead).toHaveProperty('en', dataMock.serviceWithoutSomeFields.titleEn);
    });

    test('Should generate an error when updating an entry without an id', async () => {
      const res = await request.put(`/api/service/`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const traslationTitle = new Internalization({
        key: dataMock.dbData.title,
        en: dataMock.fullService.titleEn,
        ru: dataMock.fullService.titleRu,
        ua: dataMock.fullService.titleUa,
      });
      await traslationTitle.save();

      const traslationDecs = new Internalization({
        key: dataMock.dbData.description,
        en: dataMock.fullService.descEn,
        ru: dataMock.fullService.descRu,
        ua: dataMock.fullService.descUa,
      });
      await traslationDecs.save();

      const service = new Services(dataMock.dbData);
      await service.save();

      const res = await request.delete(`/api/service/${service._id}`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('message');

      const traslationTitleAfter = await Internalization.findOne({ key: service.title });
      const traslationDescAfter = await Internalization.findOne({ key: service.description });
      const serviceAfter = await Services.findById(service._id);

      expect(!!traslationTitleAfter).toBe(false);
      expect(!!traslationDescAfter).toBe(false);
      expect(!!serviceAfter).toBe(false);
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request.put(`/api/service/`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
