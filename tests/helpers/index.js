const fs = require('fs');
const path = require('path');

const usersMock = require('../mock/users');
const { pathRootDir } = require('../../middleware/multer');

exports.auth = async request => {
  await request
    .post('/api/auth/register')
    .send(usersMock.firstUserMock)
    .set('Accept', 'application/json');

  const res = await request
    .post('/api/auth/login')
    .send(usersMock.firstUserMock)
    .set('Accept', 'application/json');

  return res.body.token;
};

const expectDataToEqual = (newData, oldData) => {
  expect(newData).toEqual({ ...newData, ...oldData });
};

exports.expectDBDataToEqual = (dbData, mock) => {
  expectDataToEqual(dbData._doc, mock);
};

exports.expectResDataToEqual = (resData, mock) => {
  expectDataToEqual(resData, mock);
};

exports.copyFile = async (readFile, writeFileName) => {
  const file = readFile.replace(/^(\.?\/)/, '').split(/\//);
  await fs
    .createReadStream(path.join(__dirname, '..', ...file))
    .pipe(fs.createWriteStream(path.join(pathRootDir, writeFileName)));
};
