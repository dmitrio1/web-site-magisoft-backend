const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

const mongod = new MongoMemoryServer();

// ---------- for func 'deleteWorkDir' ----------------
const fs = require('fs');
const path = require('path');
const util = require('util');

const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);

const { pathRootDir } = require('../../middleware/multer');
// ----------------------------------------------------

/**
 * Connect to the in-memory database.
 */
module.exports.connectDatabase = async () => {
  const uri = await mongod.getConnectionString();

  const mongooseOpts = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  };
  mongoose.promise = global.Promise;

  await mongoose.connect(uri, mongooseOpts);
};

/**
 * Drop database, close the connection and stop mongod.
 */
module.exports.closeDatabase = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongod.stop();
};

/**
 * Remove all the data for all db collections.
 */
module.exports.clearDatabase = async (isUsers = false) => {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    if (isUsers || collectionName !== 'users') await collection.deleteMany();
  }
};

module.exports.dbCollections = mongoose.connection.collections;

/**
 * Remove all file and directory what used for tests.
 */
module.exports.deleteWorkDir = async () => {
  if (fs.existsSync(pathRootDir)) {
    const files = await readdir(pathRootDir);
    const unlinkPromises = files.map(filename => unlink(path.join(pathRootDir, filename)));
    await Promise.all(unlinkPromises);
    await fs.rmdir(pathRootDir, () => {});
  }
};
