const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const User = require('../models/User');
const usersMock = require('./mock/users');

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
});

describe('User router', () => {
  it('Should create a new user', async done => {
    const res = await request
      .post('/api/auth/register')
      .send(usersMock.firstUserMock)
      .set('Accept', 'application/json');

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty('message', 'Success!');

    const user = await User.findOne({ email: usersMock.firstUserMock.email });
    expect(!!user).toEqual(true);

    done();
  });

  it('Should generate error in registration for existing user', async () => {
    const res = await request
      .post('/api/auth/register')
      .send(usersMock.firstUserMock)
      .set('Accept', 'application/json');

    expect(res.statusCode).toEqual(409);
    expect(res.body).toHaveProperty('message', 'Email already exist!');
  });

  it('Should login', async () => {
    const res = await request
      .post('/api/auth/login')
      .send(usersMock.firstUserMock)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('userId');
    expect(res.body).toHaveProperty('token');
  });

  it('Should generate error in login route for does not existing user', async () => {
    const res = await request
      .post('/api/auth/login')
      .send(usersMock.secondUserMock)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    expect(res.statusCode).toEqual(401);
    expect(res.body).toHaveProperty('message', 'Email or password are wrong');
  });

  it('Should tell what the user is authenticated', async () => {
    const resLogin = await request
      .post('/api/auth/login')
      .send(usersMock.firstUserMock)
      .set('Accept', 'application/json');

    const res = await request
      .get('/api/auth/checkToken')
      .set('Authorization', `Bearer ${resLogin.body.token}`)
      .expect('Content-Type', /json/);

    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('authenticate', true);
  });

  it("Should tell what the user isn'n authenticated", async () => {
    const res = await request.get('/api/auth/checkToken');

    expect(res.statusCode).toEqual(401);
    expect(res.body).toHaveProperty('message', 'Access denied');
  });
});
