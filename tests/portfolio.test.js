const path = require('path');
const fs = require('fs');

const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const dataMock = require('./mock/portfolio');
const Portfolio = require('../models/Portfolio');
const Internalization = require('../models/Internalization');

const helpers = require('./helpers');

const { pathRootDir } = require('../middleware/multer');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
  await dataHandler.deleteWorkDir();
});

describe('Portfolio router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/portfolio');

      expect(res.statusCode).toBe(200);
      expect(res.body).toEqual([]);
    });

    test('Should get an array with some data', async () => {
      const portfolio = new Portfolio(dataMock.dbData);
      await portfolio.save();

      const res = await request.get('/api/portfolio');

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveLength(1);
      expect(res.body[0]).toHaveProperty('description', dataMock.dbData.description);
      expect(res.body[0]).toHaveProperty('logo', dataMock.dbData.logo);
      expect(res.body[0]).toHaveProperty('slide', dataMock.dbData.slide);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/portfolio')
        .set('Content-Type', 'multipart/form-data')
        .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
        .attach('slide', path.join(__dirname, 'mock', 'mockPng.png'))
        .field('descEn', dataMock.fullData.descEn)
        .field('descRu', dataMock.fullData.descRu)
        .field('descUa', dataMock.fullData.descUa)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty('description');
      expect(res.body).toHaveProperty('logo');
      expect(res.body).toHaveProperty('slide');

      const traslation = await Internalization.findOne({ key: res.body.description });
      expect(traslation).toHaveProperty('en', dataMock.fullData.descEn);
      expect(traslation).toHaveProperty('ru', dataMock.fullData.descRu);
      expect(traslation).toHaveProperty('ua', dataMock.fullData.descUa);

      expect(fs.existsSync(path.join(pathRootDir, res.body.logo))).toBe(true);
      expect(fs.existsSync(path.join(pathRootDir, res.body.slide))).toBe(true);
    });

    // test('Should generate an error when some necessary data does not exist', async () => {
    //   const res = await request
    //     .post('/api/portfolio')
    //     .set('Content-Type', 'multipart/form-data')
    //     .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
    //     .field('descEn', dataMock.fullData.descEn)
    //     .field('descRu', dataMock.fullData.descRu)
    //     .field('descUa', dataMock.fullData.descUa)
    //     .set('Authorization', `Bearer ${userToken}`);

    //   expect(res.statusCode).toBe(422);
    //   expect(res.body).toHaveProperty('message');
    // });
  });

  describe('PUT', () => {
    test('Should update the entry in the database', async () => {
      const traslation = new Internalization({
        key: dataMock.dbData.description,
        en: dataMock.fullData.descEn,
        ru: dataMock.fullData.descRu,
        ua: dataMock.fullData.descUa,
      });
      await traslation.save();

      const portfolio = new Portfolio(dataMock.dbData);
      await portfolio.save();

      const res = await request
        .put(`/api/portfolio/${portfolio._id}`)
        .set('Content-Type', 'multipart/form-data')
        .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
        .field('descEn', dataMock.dataWitoutSomeFilds.descEn)
        .field('descUa', dataMock.dataWitoutSomeFilds.descUa)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('description');
      expect(res.body).toHaveProperty('logo');
      expect(res.body).toHaveProperty('slide');

      const traslationRead = await Internalization.findOne({ key: res.body.description });
      expect(traslationRead).toHaveProperty('en', dataMock.dataWitoutSomeFilds.descEn);
      expect(traslationRead).toHaveProperty('ua', dataMock.dataWitoutSomeFilds.descUa);

      expect(fs.existsSync(path.join(pathRootDir, res.body.logo))).toBe(true);
    });

    test('Should generate an error when updating an entry without an id', async () => {
      const res = await request
        .put(`/api/portfolio/`)
        .set('Content-Type', 'multipart/form-data')
        .attach('logo', path.join(__dirname, 'mock', 'mockPng.png'))
        .field('descEn', dataMock.dataWitoutSomeFilds.descEn)
        .field('descUa', dataMock.dataWitoutSomeFilds.descUa)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const traslation = new Internalization({
        key: dataMock.dbData.description,
        en: dataMock.fullData.descEn,
        ru: dataMock.fullData.descRu,
        ua: dataMock.fullData.descUa,
      });
      await traslation.save();

      const portfolio = new Portfolio(dataMock.dbData);
      await portfolio.save();

      await helpers.copyFile('./mock/mockPng.png', dataMock.dbData.logo);
      await helpers.copyFile('./mock/mockPng.png', dataMock.dbData.slide);

      const res = await request.delete(`/api/portfolio/${portfolio._id}`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('message');

      const traslationAfter = await Internalization.findOne({ key: portfolio.description });
      const portfolioAfter = await Portfolio.findById(portfolio._id);

      expect(!!portfolioAfter).toBe(false);
      expect(!!traslationAfter).toBe(false);

      expect(fs.existsSync(path.join(pathRootDir, dataMock.dbData.logo))).toBe(false);
      expect(fs.existsSync(path.join(pathRootDir, dataMock.dbData.slide))).toBe(false);
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request.put(`/api/portfolio/`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
