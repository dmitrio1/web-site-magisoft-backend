const supertest = require('supertest');
const path = require('path');

const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const dataMock = require('./mock/jobApplication');
const JobApplication = require('../models/JobApplication');

const helpers = require('./helpers');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
  await dataHandler.deleteWorkDir();
});

describe('Job application router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/jobApplication').set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toEqual([]);
    });

    test('Should get an array with some data', async () => {
      const application = new JobApplication({ ...dataMock.fullJA, vacancy: dataMock.fullJA.vacancyId });
      await application.save();

      const res = await request.get('/api/jobApplication').set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveLength(1);
      expect(res.body[0]).toHaveProperty('name', dataMock.fullJA.name);
      expect(res.body[0]).toHaveProperty('email', dataMock.fullJA.email);
      expect(res.body[0]).toHaveProperty('message', dataMock.fullJA.message);
      expect(res.body[0]).toHaveProperty('cv');
      expect(res.body[0]).toHaveProperty('vacancy');
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/jobApplication')
        .set('Content-Type', 'multipart/form-data')
        .attach('cv', path.join(__dirname, 'mock', 'mockPdf.pdf'))
        .field('name', dataMock.fullJA.name)
        .field('email', dataMock.fullJA.email)
        .field('message', dataMock.fullJA.message)
        .field('vacancyId', dataMock.fullJA.vacancyId);

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty('message');
    });

    // test('Should generate an error when some necessary data does not exist', async () => {
    //   const res = await request
    //     .post('/api/jobApplication')
    //     .set('Content-Type', 'multipart/form-data')
    //     .attach('cv', path.join(__dirname, 'mock', 'mockPdf.pdf'))
    //     .field('name', dataMock.fullJA.name)
    //     .field('email', dataMock.fullJA.email)
    //     .field('message', dataMock.fullJA.message)
    //     .field('vacancyId', dataMock.fullJA.vacancyId);

    //   expect(res.statusCode).toBe(201);
    //   expect(res.body).toHaveProperty('message');
    // }); // написать тест после созания валидации на этот роут
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      // copy file
      await helpers.copyFile('./mock/mockPdf.pdf', dataMock.fullJA.cv);

      // write in db
      const application = new JobApplication({ ...dataMock.fullJA, vacancy: dataMock.fullJA.vacancyId });
      await application.save();

      // send request
      const res = await request
        .delete(`/api/jobApplication/${application._id}`)
        .set('Authorization', `Bearer ${userToken}`);

      // watch response
      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('message');
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request
        .delete(`/api/clientContacts/5e42bf0c87099a741e53b23f`)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
