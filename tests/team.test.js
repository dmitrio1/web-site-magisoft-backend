const path = require('path');
const fs = require('fs');

const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const Team = require('../models/Team');
const Internalization = require('../models/Internalization');

const request = supertest(app);

const helpers = require('./helpers');
const dataMock = require('./mock/team');

const { pathRootDir } = require('../middleware/multer');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
  await dataHandler.deleteWorkDir();
});

describe('Team router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/team');

      expect(res.statusCode).toBe(200);
      expect(res.body).toEqual([]);
    });

    test('Should get an array with some data', async () => {
      const team = new Team(dataMock.dbData);
      await team.save();

      const res = await request.get('/api/team');

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveLength(1);
      expect(res.body[0]).toHaveProperty('name', dataMock.dbData.name);
      expect(res.body[0]).toHaveProperty('position', dataMock.dbData.position);
      expect(res.body[0]).toHaveProperty('image', dataMock.dbData.image);
      expect(res.body[0]).toHaveProperty('location', dataMock.dbData.location);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/team')
        .set('Content-Type', 'multipart/form-data')
        .field('nameEn', dataMock.fullData.nameEn)
        .field('nameRu', dataMock.fullData.nameRu)
        .field('nameUa', dataMock.fullData.nameUa)
        .field('positionEn', dataMock.fullData.positionEn)
        .field('positionRu', dataMock.fullData.positionRu)
        .field('positionUa', dataMock.fullData.positionUa)
        .attach('image', path.join(__dirname, 'mock', 'mockPng.png'))
        .field('location', dataMock.fullData.location)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty('name');
      expect(res.body).toHaveProperty('position');
      expect(res.body).toHaveProperty('image');
      expect(res.body).toHaveProperty('location');

      const traslationName = await Internalization.findOne({ key: res.body.name });
      expect(traslationName).toHaveProperty('en', dataMock.fullData.nameEn);
      expect(traslationName).toHaveProperty('ru', dataMock.fullData.nameRu);
      expect(traslationName).toHaveProperty('ua', dataMock.fullData.nameUa);

      const traslationPos = await Internalization.findOne({ key: res.body.position });
      expect(traslationPos).toHaveProperty('en', dataMock.fullData.positionEn);
      expect(traslationPos).toHaveProperty('ru', dataMock.fullData.positionRu);
      expect(traslationPos).toHaveProperty('ua', dataMock.fullData.positionUa);

      expect(fs.existsSync(path.join(pathRootDir, res.body.image))).toBe(true);
    });
  });

  describe('PUT', () => {
    test('Should update the entry in the database', async () => {
      const traslationName = new Internalization({
        key: dataMock.dbData.name,
        en: dataMock.fullData.nameEn,
        ru: dataMock.fullData.nameRu,
        ua: dataMock.fullData.nameEn,
      });
      await traslationName.save();

      const traslationPos = new Internalization({
        key: dataMock.dbData.position,
        en: dataMock.fullData.positionEn,
        ru: dataMock.fullData.positionRu,
        ua: dataMock.fullData.positionUa,
      });
      await traslationPos.save();

      const employee = new Team(dataMock.dbData);
      await employee.save();

      const res = await request
        .put(`/api/team/${employee._id}`)
        .set('Content-Type', 'multipart/form-data')
        .attach('image', path.join(__dirname, 'mock', 'mockPng.png'))
        .field('nameEn', dataMock.dataWithoutSomeFilds.nameEn)
        .field('positionUa', dataMock.dataWithoutSomeFilds.positionUa)
        .field('location', dataMock.dataWithoutSomeFilds.location)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('name');
      expect(res.body).toHaveProperty('position');
      expect(res.body).toHaveProperty('image');
      expect(res.body).toHaveProperty('location', dataMock.dataWithoutSomeFilds.location);

      const traslationNameAfter = await Internalization.findOne({ key: res.body.name });
      expect(traslationNameAfter).toHaveProperty('en', dataMock.dataWithoutSomeFilds.nameEn);

      const traslationPosAfter = await Internalization.findOne({ key: res.body.position });
      expect(traslationPosAfter).toHaveProperty('ua', dataMock.dataWithoutSomeFilds.positionUa);

      expect(fs.existsSync(path.join(pathRootDir, res.body.image))).toBe(true);
    });

    test('Should generate an error when updating an entry without an id', async () => {
      const res = await request
        .put(`/api/team/`)
        .set('Content-Type', 'multipart/form-data')
        .attach('image', path.join(__dirname, 'mock', 'mockPng.png'))
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const traslationName = new Internalization({
        key: dataMock.dbData.name,
        en: dataMock.fullData.nameEn,
        ru: dataMock.fullData.nameRu,
        ua: dataMock.fullData.nameEn,
      });
      await traslationName.save();

      const traslationPos = new Internalization({
        key: dataMock.dbData.position,
        en: dataMock.fullData.positionEn,
        ru: dataMock.fullData.positionRu,
        ua: dataMock.fullData.positionUa,
      });
      await traslationPos.save();

      const employee = new Team(dataMock.dbData);
      await employee.save();

      await helpers.copyFile('./mock/mockPng.png', dataMock.dbData.image);

      const res = await request.delete(`/api/team/${employee._id}`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('message');

      const traslationNameAfter = await Internalization.findOne({ key: employee.name });
      const traslationPosAfter = await Internalization.findOne({ key: employee.position });
      const employeeAfter = await Team.findById(employee._id);

      expect(!!traslationNameAfter).toBe(false);
      expect(!!traslationPosAfter).toBe(false);
      expect(!!employeeAfter).toBe(false);

      expect(fs.existsSync(path.join(pathRootDir, employee.image))).toBe(false);
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request.put(`/api/team/`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
