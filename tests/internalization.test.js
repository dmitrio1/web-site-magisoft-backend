const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const mockData = require('./mock/internalization');
const Internalization = require('../models/Internalization');

const helpers = require('./helpers');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
});

describe('Internalization router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/internalization').expect('Content-Type', /json/);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toEqual(mockData.resEmptyTranslationData);
    });

    test('Should get an array with some data', async () => {
      const translation = new Internalization(mockData.fullTranslation);
      await translation.save();

      const res = await request.get('/api/internalization').expect('Content-Type', /json/);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toEqual(mockData.resTranslationData);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/internalization')
        .send(mockData.fullTranslation)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(201);
      helpers.expectResDataToEqual(res.body, mockData.fullTranslation);

      const translation = await Internalization.findOne(mockData.fullTranslation);
      helpers.expectDBDataToEqual(translation, mockData.fullTranslation);
    });

    test('Should generate an error when some necessary data does not exist', async () => {
      const res = await request
        .post('/api/internalization')
        .send(mockData.translationWithoutKey)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty('message');
    });

    test('Should generate an error when an entry with this key in the database already exists', async () => {
      const translation = new Internalization(mockData.fullTranslation);
      await translation.save();

      const res = await request
        .post('/api/internalization')
        .send(mockData.fullTranslation)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(409);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('PUT', () => {
    test('Should update the entry in the database', async () => {
      const translation = new Internalization(mockData.fullTranslation);
      await translation.save();

      const res = await request
        .put('/api/internalization')
        .send(mockData.translationWithoutSomeFields)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(200);
      helpers.expectResDataToEqual(res.body, mockData.translationWithoutSomeFields);
    });

    test('Should generate an error when updating an entry without a key', async () => {
      const res = await request
        .put('/api/internalization')
        .send(mockData.translationWithoutKey)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const translation = new Internalization(mockData.fullTranslation);
      await translation.save();

      const res = await request
        .delete(`/api/internalization/${mockData.fullTranslation.key}`)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('message');
    });

    test('Should generate an error when deleting an entry with the wrong key', async () => {
      const res = await request
        .delete(`/api/internalization/${mockData.fullTranslation.key}`)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });

    test('Should generate an error when deleting an entry without key', async () => {
      const res = await request.delete(`/api/internalization`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
