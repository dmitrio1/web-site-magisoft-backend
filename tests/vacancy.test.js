const supertest = require('supertest');
const app = require('../app');
const dataHandler = require('./data');

const request = supertest(app);

const dataMock = require('./mock/vacancy');
const Vacancy = require('../models/Vacancy');
const Internalization = require('../models/Internalization');

const helpers = require('./helpers');

let userToken = '';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dataHandler.connectDatabase();

  userToken = await helpers.auth(request);
});

afterEach(async () => {
  await dataHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dataHandler.closeDatabase();
});

describe('Vacancy router', () => {
  describe('GET', () => {
    test('Should get an array without any data', async () => {
      const res = await request.get('/api/vacancy').expect('Content-Type', /json/);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toEqual([]);
    });

    test('Should get an array with some data', async () => {
      const vacancy = new Vacancy(dataMock.dbData);
      await vacancy.save();

      const res = await request.get('/api/vacancy').expect('Content-Type', /json/);

      expect(res.statusCode).toEqual(200);
      helpers.expectResDataToEqual(res.body[0], dataMock.dbData);
    });
  });

  describe('POST', () => {
    test('Should create a new entry in the database', async () => {
      const res = await request
        .post('/api/vacancy')
        .send(dataMock.fullData)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(201);
      expect(res.body).toHaveProperty('name', dataMock.fullData.name);
      expect(res.body).toHaveProperty('requirements');
      expect(res.body).toHaveProperty('willBePlus');
      expect(res.body).toHaveProperty('responsibilities');
      expect(res.body).toHaveProperty('conditions');
      expect(res.body).toHaveProperty('isEnabled', dataMock.fullData.isEnabled);
      expect(res.body).toHaveProperty('specialization', dataMock.fullData.specialization);

      const traslationReq = await Internalization.findOne({ key: res.body.requirements });
      expect(traslationReq).toHaveProperty('en', dataMock.fullData.requirementsEn);
      expect(traslationReq).toHaveProperty('ru', dataMock.fullData.requirementsRu);
      expect(traslationReq).toHaveProperty('ua', dataMock.fullData.requirementsUa);

      const traslationWBP = await Internalization.findOne({ key: res.body.willBePlus });
      expect(traslationWBP).toHaveProperty('en', dataMock.fullData.willBePlusEn);
      expect(traslationWBP).toHaveProperty('ru', dataMock.fullData.willBePlusRu);
      expect(traslationWBP).toHaveProperty('ua', dataMock.fullData.willBePlusUa);

      const traslationRes = await Internalization.findOne({ key: res.body.responsibilities });
      expect(traslationRes).toHaveProperty('en', dataMock.fullData.responsibilitiesEn);
      expect(traslationRes).toHaveProperty('ru', dataMock.fullData.responsibilitiesRu);
      expect(traslationRes).toHaveProperty('ua', dataMock.fullData.responsibilitiesUa);

      const traslationCon = await Internalization.findOne({ key: res.body.conditions });
      expect(traslationCon).toHaveProperty('en', dataMock.fullData.conditionsEn);
      expect(traslationCon).toHaveProperty('ru', dataMock.fullData.conditionsRu);
      expect(traslationCon).toHaveProperty('ua', dataMock.fullData.conditionsUa);
    });

    test('Should generate an error when some necessary data does not exist', async () => {
      const res = await request
        .post('/api/vacancy')
        .send(dataMock.fullDataWithoutSpecialization)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(422);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('PUT', () => {
    test('Should update the entry in the database', async () => {
      const traslationReq = new Internalization({
        key: dataMock.dbData.requirements,
        en: dataMock.fullData.requirementsEn,
        ru: dataMock.fullData.requirementsRu,
        ua: dataMock.fullData.requirementsUa,
      });
      const traslationWBP = new Internalization({
        key: dataMock.dbData.willBePlus,
        en: dataMock.fullData.willBePlusEn,
        ru: dataMock.fullData.willBePlusRu,
        ua: dataMock.fullData.willBePlusUa,
      });
      const traslationRes = new Internalization({
        key: dataMock.dbData.responsibilities,
        en: dataMock.fullData.responsibilitiesEn,
        ru: dataMock.fullData.responsibilitiesRu,
        ua: dataMock.fullData.responsibilitiesUa,
      });
      const traslationCon = new Internalization({
        key: dataMock.dbData.conditions,
        en: dataMock.fullData.conditionsEn,
        ru: dataMock.fullData.conditionsRu,
        ua: dataMock.fullData.conditionsUa,
      });
      await traslationReq.save();
      await traslationWBP.save();
      await traslationRes.save();
      await traslationCon.save();

      const vacancy = new Vacancy(dataMock.dbData);
      await vacancy.save();

      const res = await request
        .put(`/api/vacancy/${vacancy._id}`)
        .send(dataMock.dataWithoutSomeFields)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('name', dataMock.dataWithoutSomeFields.name);
      expect(res.body).toHaveProperty('isEnabled', dataMock.dataWithoutSomeFields.isEnabled);

      const traslationReqAfter = await Internalization.findOne({ key: res.body.requirements });
      const traslationWBPAfter = await Internalization.findOne({ key: res.body.willBePlus });
      const traslationResAfter = await Internalization.findOne({ key: res.body.responsibilities });
      const traslationConAfter = await Internalization.findOne({ key: res.body.conditions });
      const vacancyAfter = await Vacancy.findById(vacancy._id);

      expect(vacancyAfter).toHaveProperty('name', dataMock.dataWithoutSomeFields.name);
      expect(vacancyAfter).toHaveProperty('isEnabled', dataMock.dataWithoutSomeFields.isEnabled);

      expect(traslationReqAfter).toHaveProperty('en', dataMock.dataWithoutSomeFields.requirementsEn);
      expect(traslationWBPAfter).toHaveProperty('ua', dataMock.dataWithoutSomeFields.willBePlusUa);
      expect(traslationResAfter).toHaveProperty('en', dataMock.dataWithoutSomeFields.responsibilitiesEn);
      expect(traslationConAfter).toHaveProperty('ru', dataMock.dataWithoutSomeFields.conditionsRu);
    });

    test('Should generate an error when updating an entry without an id', async () => {
      const res = await request
        .put('/api/vacancy')
        .send(dataMock.dataWithoutSomeFields)
        .set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });
  });

  describe('DELETE', () => {
    test('Should delete the entry in database', async () => {
      const traslationReq = new Internalization({
        key: dataMock.dbData.requirements,
        en: dataMock.fullData.requirementsEn,
        ru: dataMock.fullData.requirementsRu,
        ua: dataMock.fullData.requirementsUa,
      });
      const traslationWBP = new Internalization({
        key: dataMock.dbData.willBePlus,
        en: dataMock.fullData.willBePlusEn,
        ru: dataMock.fullData.willBePlusRu,
        ua: dataMock.fullData.willBePlusUa,
      });
      const traslationRes = new Internalization({
        key: dataMock.dbData.responsibilities,
        en: dataMock.fullData.responsibilitiesEn,
        ru: dataMock.fullData.responsibilitiesRu,
        ua: dataMock.fullData.responsibilitiesUa,
      });
      const traslationCon = new Internalization({
        key: dataMock.dbData.conditions,
        en: dataMock.fullData.conditionsEn,
        ru: dataMock.fullData.conditionsRu,
        ua: dataMock.fullData.conditionsUa,
      });
      await traslationReq.save();
      await traslationWBP.save();
      await traslationRes.save();
      await traslationCon.save();

      const vacancy = new Vacancy(dataMock.dbData);
      await vacancy.save();

      const res = await request.delete(`/api/vacancy/${vacancy._id}`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toBe(200);
      expect(res.body).toHaveProperty('message');

      const traslationReqAfter = await Internalization.findOne({ key: res.body.requirements });
      const traslationWBPAfter = await Internalization.findOne({ key: res.body.willBePlus });
      const traslationResAfter = await Internalization.findOne({ key: res.body.responsibilities });
      const traslationConAfter = await Internalization.findOne({ key: res.body.conditions });
      const vacancyAfter = await Vacancy.findById(vacancy._id);

      expect(!!traslationReqAfter).toBe(false);
      expect(!!traslationWBPAfter).toBe(false);
      expect(!!traslationResAfter).toBe(false);
      expect(!!traslationConAfter).toBe(false);
      expect(!!vacancyAfter).toBe(false);
    });

    test('Should generate an error when deleting an entry with the wrong id', async () => {
      const res = await request.delete(`/api/vacancy/`).set('Authorization', `Bearer ${userToken}`);

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('message');
    });
  });
});
