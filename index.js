const mongoose = require('mongoose');
const toJson = require('@meanie/mongoose-to-json');
const mongodbErrorHandler = require('mongoose-mongodb-errors');

// require('custom-env').env();

// _id to id and delete _v...
mongoose.plugin(toJson);
mongoose.plugin(mongodbErrorHandler);

const app = require('./app');

async function start() {
  try {
    await mongoose.connect(process.env.DATABASE, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    });

    mongoose.Promise = global.Promise;

    app.set('port', process.env.PORT || 7000);

    const server = app.listen(app.get('port'), () => {
      console.log(`Server is running on port ${server.address().port}`);
    });
  } catch (e) {
    console.error(`🚫 ➞ ${e.message}`, {
      env: JSON.stringify(process.env.NODE_ENV, null, 2),
    });
  }
}

start();
