const router = require('express').Router();
// const { body } = require('express-validator');
const controllers = require('../controllers/jobApplicationController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.authorize, middleware.catchAsyncErrors(controllers.getAllApplications))
  .post(
    // middleware.validate([
    //   body('email')
    //     .isEmail()
    //     .normalizeEmail(),
    //   body('message')
    //     .trim()
    //     .notEmpty()
    //     .withMessage('Message should not be empty!'),
    // ]),
    middleware.multer.errorHandler(middleware.multer.upload.fileFilter(['.pdf', '.doc', '.docx']).single('cv')),
    middleware.catchAsyncErrors(controllers.createApplication)
  );

router.delete(
  '/:id',
  middleware.authorize,
  middleware.reqIdIsMongoId,
  middleware.catchAsyncErrors(controllers.deleteJobApplications)
);

module.exports = router;
