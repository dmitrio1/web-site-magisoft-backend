const router = require('express').Router();
const controllers = require('../controllers/teamController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.catchAsyncErrors(controllers.getAllTeam))
  .post(
    middleware.authorize,
    middleware.multer.errorHandler(middleware.multer.upload.fileFilter(['.png', '.jpg', '.jpeg']).single('image')),
    middleware.catchAsyncErrors(controllers.createEmployee)
  );

router
  .route('/:id')
  .all(middleware.authorize, middleware.reqIdIsMongoId)
  .put(
    middleware.multer.errorHandler(middleware.multer.upload.fileFilter(['.png', '.jpg', '.jpeg']).single('image')),
    middleware.catchAsyncErrors(controllers.updateEmployee)
  )
  .delete(middleware.catchAsyncErrors(controllers.deleteEmployee));

module.exports = router;
