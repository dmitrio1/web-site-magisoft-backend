const router = require('express').Router();
const controllers = require('../controllers/internalizationController');
const middleware = require('../middleware');

router.get('/', middleware.catchAsyncErrors(controllers.getAllTranslations));

router
  .route('/')
  .all(middleware.authorize)
  .post(middleware.catchAsyncErrors(controllers.createTranslation))
  .put(middleware.catchAsyncErrors(controllers.updateTranslation));

router.delete('/:key', middleware.authorize, middleware.catchAsyncErrors(controllers.deleteTranslation));

module.exports = router;
