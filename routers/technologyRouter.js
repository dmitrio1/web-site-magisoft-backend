const router = require('express').Router();
const controllers = require('../controllers/technologyController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.catchAsyncErrors(controllers.getAllTechnologies))
  .post(
    middleware.authorize,
    middleware.multer.errorHandler(middleware.multer.upload.fileFilter(['.png', '.jpg', '.jpeg']).single('logo')),
    middleware.catchAsyncErrors(controllers.createTechnology)
  );

router
  .route('/:id')
  .all(middleware.authorize, middleware.reqIdIsMongoId)
  .put(
    middleware.multer.errorHandler(middleware.multer.upload.fileFilter(['.png', '.jpg', '.jpeg']).single('logo')),
    middleware.catchAsyncErrors(controllers.updateTechnology)
  )
  .delete(middleware.authorize, middleware.catchAsyncErrors(controllers.deleteTechnology));

module.exports = router;
