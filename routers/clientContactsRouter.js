const router = require('express').Router();
const { body } = require('express-validator');
const controllers = require('../controllers/clientContactsController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.authorize, middleware.catchAsyncErrors(controllers.getAllClientContacts))
  .post(
    // middleware.validate([
    //   body('email')
    //     .isEmail()
    //     .normalizeEmail(),
    //   body('message')
    //     .trim()
    //     .notEmpty()
    //     .withMessage('Message should not be empty!'),
    // ]),
    middleware.catchAsyncErrors(controllers.createClientContacts)
  );

router.delete(
  '/:id',
  middleware.authorize,
  middleware.reqIdIsMongoId,
  middleware.catchAsyncErrors(controllers.deleteClientContacts)
);

module.exports = router;
