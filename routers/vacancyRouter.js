const router = require('express').Router();
const controllers = require('../controllers/vacancyController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.catchAsyncErrors(controllers.getAllVacancies))
  .post(middleware.authorize, middleware.catchAsyncErrors(controllers.createVacancy));

router
  .route('/:id')
  .all(middleware.authorize, middleware.reqIdIsMongoId)
  .put(middleware.catchAsyncErrors(controllers.updateVacancy))
  .delete(middleware.catchAsyncErrors(controllers.deleteVacancy));

module.exports = router;
