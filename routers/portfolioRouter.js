const router = require('express').Router();
const controllers = require('../controllers/portfolioController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.catchAsyncErrors(controllers.getPortfolio))
  .post(
    middleware.authorize,
    middleware.multer.errorHandler(
      middleware.multer.upload.fileFilter(['.png', '.jpg', '.jpeg']).fields([
        { name: 'logo', maxCount: 1 },
        { name: 'slide', maxCount: 1 },
      ])
    ),
    middleware.catchAsyncErrors(controllers.createProduct)
  );

router
  .route('/:id')
  .all(middleware.authorize, middleware.reqIdIsMongoId)
  .put(
    middleware.multer.errorHandler(
      middleware.multer.upload.fileFilter(['.png', '.jpg', '.jpeg']).fields([
        { name: 'logo', maxCount: 1 },
        { name: 'slide', maxCount: 1 },
      ])
    ),
    middleware.catchAsyncErrors(controllers.updateProduct)
  )
  .delete(middleware.catchAsyncErrors(controllers.deleteProduct));

module.exports = router;
