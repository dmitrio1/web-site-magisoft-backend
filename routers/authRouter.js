const router = require('express').Router();
const { body } = require('express-validator');
const controllers = require('../controllers/authController');
const middleware = require('../middleware');

router.route('/register').post(
  // middleware.validate([
  //   body('email')
  //     .isEmail()
  //     .normalizeEmail(),
  //   body('password')
  //     .trim()
  //     .matches(/^\S*$/, 'g')
  //     .withMessage('field should not include spaces')
  //     .isLength({ min: 6, max: 1024 })
  //     .withMessage('must be at least 6 chars long'),
  // ]),
  middleware.catchAsyncErrors(controllers.register)
);

router.route('/login').post(
  // middleware.validate([
  //   body('email')
  //     .isEmail()
  //     .normalizeEmail(),
  //   body('password')
  //     .trim()
  //     .matches(/^\S*$/, 'g')
  //     .withMessage('field should not include spaces')
  //     .isLength({ min: 6, max: 1024 })
  //     .withMessage('must be at least 6 chars long'),
  // ]),
  controllers.login
);

router.route('/checkToken').get(middleware.authorize, (req, res) => res.status(200).json({ authenticate: true }));

module.exports = router;
