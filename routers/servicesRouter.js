const router = require('express').Router();
const { body } = require('express-validator');
const controllers = require('../controllers/servicesController');
const middleware = require('../middleware');

router
  .route('/')
  .get(middleware.catchAsyncErrors(controllers.getAllServises))
  .post(
    middleware.authorize,
    // middleware.validate([
    //   body('title')
    //     .trim()
    //     .isLength({ min: 2, max: 1024 })
    //     .withMessage('Title must be at least 2 chars long'),
    //   body('description')
    //     .trim()
    //     .isLength({ min: 2, max: 1024 })
    //     .withMessage('Description must be at least 2 chars long'),
    // ]),
    middleware.catchAsyncErrors(controllers.createServise)
  );

router
  .route('/:id')
  .all(middleware.authorize, middleware.reqIdIsMongoId)
  .put(
    // middleware.validate([
    //   body('title')
    //     .trim()
    //     .isLength({ min: 2, max: 1024 })
    //     .withMessage('Title must be at least 2 chars long'),
    //   body('description')
    //     .trim()
    //     .isLength({ min: 2, max: 1024 })
    //     .withMessage('Description must be at least 2 chars long'),
    // ]),
    middleware.catchAsyncErrors(controllers.updateServise)
  )
  .delete(middleware.catchAsyncErrors(controllers.deleteServise));

module.exports = router;
