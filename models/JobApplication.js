const mongoose = require('mongoose');

const JobApplicationsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
    },
    message: {
      type: String,
      required: true,
      trim: true,
    },
    cv: {
      type: String,
      required: true,
      trim: true,
    },
    vacancy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Vacancy',
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('JobApplications', JobApplicationsSchema);
