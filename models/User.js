const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      unique: true,
      lowercase: true,
      trim: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.statics.findUserByEmail = function(email) {
  return this.findOne({ email });
};

userSchema.methods.validatePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

userSchema.methods.hashPassword = function(password) {
  const salt = bcrypt.genSaltSync(10);
  this.password = bcrypt.hashSync(password, salt);
};

userSchema.methods.generateAcessJWT = function() {
  return jwt.sign({ email: this.email, userId: this._id }, process.env.SECRET, {
    expiresIn: '7d',
  });
};

userSchema.methods.generateRefreshJWT = function() {
  return jwt.sign({ email: this.email, userId: this._id }, process.env.REFRESH_TOKET_SECRET, {
    expiresIn: 86400,
  });
};

userSchema.methods.toAuthJSON = function() {
  return {
    userId: this._id,
    token: this.generateAcessJWT(),
    // refreshToken: this.generateRefreshJWT()
  };
};

module.exports = mongoose.model('User', userSchema);
