const mongoose = require('mongoose');

const vacancySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  requirements: {
    type: String,
    required: true,
  },
  willBePlus: {
    type: String,
    required: true,
  },
  responsibilities: {
    type: String,
    required: true,
  },
  conditions: {
    type: String,
    required: true,
  },
  specialization: {
    type: String,
    enum: ['frontend', 'backend'],
    required: true,
  },
  isEnabled: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('Vacancy', vacancySchema);
