const mongoose = require('mongoose');

const portfolioSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  logo: {
    type: String,
  },
  description: {
    type: String,
    required: true,
  },
  slide: {
    type: String,
  },
});

module.exports = mongoose.model('Portfolio', portfolioSchema);
