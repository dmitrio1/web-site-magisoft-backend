const mongoose = require('mongoose');

const internalizationSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    trim: true,
  },
  en: {
    type: String,
    default: '',
    trim: true,
  },
  ru: {
    type: String,
    default: '',
    trim: true,
  },
  ua: {
    type: String,
    default: '',
    trim: true,
  },
});

module.exports = mongoose.model('Internalization', internalizationSchema);
