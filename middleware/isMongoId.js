const HttpError = require('http-errors');
const { ObjectId } = require('mongoose').Types;

module.exports = (req, res, next) => {
  // /^[0-9a-fA-F]{24}$/.test(req.params.id)
  if (!!req.params.id && ObjectId.isValid(req.params.id)) {
    return next();
  }

  next(new HttpError[404]("Don't found!"));
};
