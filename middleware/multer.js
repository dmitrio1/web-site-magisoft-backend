const multer = require('multer');
const path = require('path');
const fs = require('fs');
const HttpError = require('http-errors');

const pathRootDir =
  process.env.NODE_ENV === 'test'
    ? path.join(__dirname, '..', 'uploads', 'testData')
    : path.join(__dirname, '..', 'uploads');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (fs.existsSync(pathRootDir)) {
      cb(null, pathRootDir);
    } else {
      // { recursive: true }, - for create dir if it wasn't created
      fs.mkdir(pathRootDir, function(err) {
        if (err) {
          cb(new Error('Some error when directory was creating'));
        } else {
          cb(null, pathRootDir);
        }
      });
    }
  },
  filename: (req, file, cb) => {
    const a = file.originalname.split('.');
    cb(
      null,
      `${req.baseUrl.replace(/(\/api\/)([a-z]+)(\D*)/, '$2')}-${file.fieldname}-${Date.now()}.${a[a.length - 1]}`
    );
  },
});

const _update = (types = []) =>
  multer({
    storage,
    fileFilter: (req, file, callback) => {
      const ext = path.extname(file.originalname);
      if (types.length && !types.some(type => type === ext)) {
        return callback(new Error('Unsupported file format'));
      }
      callback(null, true);
    },
    limits: { fileSize: 1024 * 1024 * 2 },
  });

const upload = {
  fileFilter: _update,
  allTypes: _update(),
};

const errorHandler = uploads => (req, res, next) => {
  uploads(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      // Случилась ошибка Multer при загрузке.

      // error with file's size
      if (err.code === 'LIMIT_FILE_SIZE') return next(new HttpError[400]('Reached file size limit'));

      next(new HttpError[400](err.message));
    } else if (err) {
      // При загрузке произошла неизвестная или сгенерированная нами ошибка.

      next(new HttpError[400](err.message)); // error with file's type
    }

    next();
  });
};

module.exports = { upload, errorHandler, pathRootDir };
