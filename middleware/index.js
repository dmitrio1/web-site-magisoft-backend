const errorHandlers = require('./errorHandlers');
const validation = require('./validation');
const authorize = require('./authorize');
const passportJwt = require('./passportJwt');
const passportLocal = require('./passportLocal');
const multer = require('./multer');
const reqIdIsMongoId = require('./isMongoId');

module.exports = {
  ...errorHandlers,
  ...validation,
  authorize,
  passportJwt,
  passportLocal,
  multer,
  reqIdIsMongoId,
};
