const HttpError = require('http-errors');
const EmailService = require('../utils/emailService');
const ClientContacts = require('../models/ClientContacts');

const emailService = new EmailService();

const createClientContacts = async (req, res) => {
  const contact = new ClientContacts(req.body);

  await contact.save();
  await emailService.send({
    to: process.env.MANAGERS_EMAIL,
    subject: 'Incoming offer',
    html: `
    <p><b>fullname</b>: ${req.body.fullname},</p>
    <p><b>email</b>: ${req.body.email},</p>
    <p><b>skype</b>: ${req.body.skype},</p>
    <p><b>company name</b>: ${req.body.companyName},</p>
    <p><b>project type</b>: ${req.body.projectType},</p>
    <p><b>message</b>: ${req.body.message}</p>
    `,
  });

  res.status(201).json({ message: 'Success!' });
};

const getAllClientContacts = async (req, res) => {
  const contact = await ClientContacts.find();

  res.json(contact);
};

const deleteClientContacts = async (req, res) => {
  const contact = await ClientContacts.findByIdAndDelete(req.params.id);

  if (!contact) throw HttpError[404]("A record don't found!");

  res.json({ message: 'Success!' });
};

module.exports = {
  createClientContacts,
  getAllClientContacts,
  deleteClientContacts,
};
