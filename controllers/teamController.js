const HttpError = require('http-errors');

const Team = require('../models/Team');
const Internalization = require('../models/Internalization');

const utils = require('../utils');

const createEmployee = async (req, res) => {
  if (!req.file) {
    throw HttpError[400]('Image is required!');
  }

  const keyName = `team_name_${utils.makeRandomId()}`;
  const keyPosition = `team_position_${utils.makeRandomId()}`;
  const image = req.file.filename;

  const translationName = new Internalization({
    key: keyName,
    en: req.body.nameEn,
    ru: req.body.nameRu,
    ua: req.body.nameUa,
  });
  await translationName.save();

  const translationPosition = new Internalization({
    key: keyPosition,
    en: req.body.positionEn,
    ru: req.body.positionRu,
    ua: req.body.positionUa,
  });
  await translationPosition.save();

  const employee = new Team({
    name: keyName,
    position: keyPosition,
    image,
    location: req.body.location,
  });
  await employee.save();

  res.status(201).json(employee);
};

const getAllTeam = async (req, res) => {
  const team = await Team.find();
  res.json(team);
};

const updateEmployee = async (req, res) => {
  const employee = await Team.findById(req.params.id);
  if (!employee) throw HttpError[404]("Employee don't found!");

  if (req.file) {
    utils.deleteFileIfExists(employee.image, () => {
      employee.image = req.file.filename;
    });
  }

  if (req.body.location) {
    employee.location = req.body.location;
  }

  const newNameTranslation = utils.generationObjTranslation(req.body.nameEn, req.body.nameRu, req.body.nameUa);
  const newPositionTranslation = utils.generationObjTranslation(
    req.body.positionEn,
    req.body.positionRu,
    req.body.positionUa
  );

  const translationName = await Internalization.findOneAndUpdate({ key: employee.name }, { ...newNameTranslation });
  if (!translationName) utils.newTranslationData(Internalization, employee.name, newNameTranslation);

  const translationPosition = await Internalization.findOneAndUpdate(
    { key: employee.position },
    { ...newPositionTranslation }
  );
  if (!translationPosition) utils.newTranslationData(Internalization, employee.position, newPositionTranslation);

  await employee.update(employee);

  res.json(employee);
};

const deleteEmployee = async (req, res) => {
  const employee = await Team.findByIdAndDelete(req.params.id);
  if (!employee) throw HttpError[404]("Employee don't found!");

  await Internalization.findOneAndDelete({ key: employee.name });
  await Internalization.findOneAndDelete({ key: employee.position });

  utils.deleteFileIfExists(employee.image);

  res.json({ message: 'Success!' });
};

module.exports = {
  createEmployee,
  getAllTeam,
  updateEmployee,
  deleteEmployee,
};
