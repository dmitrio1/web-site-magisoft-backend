const HttpError = require('http-errors');
const Services = require('../models/Services');
const Internalization = require('../models/Internalization');
const utils = require('../utils');

const createServise = async (req, res) => {
  const keyTitle = `service_title_${utils.makeRandomId()}`;
  const keyDesc = `service_desc_${utils.makeRandomId()}`;

  const translationTitle = new Internalization({
    key: keyTitle,
    en: req.body.titleEn,
    ru: req.body.titleRu,
    ua: req.body.titleUa,
  });
  await translationTitle.save();

  const translationDesc = new Internalization({
    key: keyDesc,
    en: req.body.descEn,
    ru: req.body.descRu,
    ua: req.body.descUa,
  });
  await translationDesc.save();

  const service = new Services({
    title: keyTitle,
    description: keyDesc,
  });

  await service.save();

  res.status(201).json(service);
};

const getAllServises = async (req, res) => {
  const servises = await Services.find();

  res.json(servises);
};

const updateServise = async (req, res) => {
  const service = await Services.findById(req.params.id);
  if (!service) throw HttpError[404]("Servise don't found!");

  const newTitileTranslation = utils.generationObjTranslation(req.body.titleEn, req.body.titleRu, req.body.titleUa);
  const newDescTranslation = utils.generationObjTranslation(req.body.descEn, req.body.descRu, req.body.descUa);

  const translationTitle = await Internalization.findOneAndUpdate({ key: service.title }, { ...newTitileTranslation });
  if (!translationTitle) utils.newTranslationData(Internalization, service.title, newTitileTranslation);

  const translationDesc = await Internalization.findOneAndUpdate(
    { key: service.description },
    { ...newDescTranslation }
  );
  if (!translationDesc) utils.newTranslationData(Internalization, service.description, newDescTranslation);

  res.json(service);
};

const deleteServise = async (req, res) => {
  const service = await Services.findByIdAndDelete(req.params.id);
  if (!service) throw HttpError[404]("Servise don't found!");

  await Internalization.findOneAndDelete({ key: service.title });
  await Internalization.findOneAndDelete({ key: service.description });

  res.json({ message: 'Success!' });
};

module.exports = {
  createServise,
  getAllServises,
  updateServise,
  deleteServise,
};
