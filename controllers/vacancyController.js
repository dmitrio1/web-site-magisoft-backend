const HttpError = require('http-errors');

const Vacancy = require('../models/Vacancy');
const Internalization = require('../models/Internalization');
const utils = require('../utils');

const createVacancy = async (req, res) => {
  if (!req.body.specialization || !/(\bfrontend\b|\bbackend\b)/.test(req.body.specialization)) {
    throw HttpError[422]('Specialization is required and must be frontend or backend');
  }

  const keyRequirements = `vacancy_requirements_${utils.makeRandomId()}`;
  const keyWillBePlus = `vacancy_willbeplus_${utils.makeRandomId()}`;
  const keyResponsibilities = `vacancy_responsibilities_${utils.makeRandomId()}`;
  const keyConditions = `vacancy_conditions_${utils.makeRandomId()}`;

  const translationRequirements = new Internalization({
    key: keyRequirements,
    en: req.body.requirementsEn,
    ru: req.body.requirementsRu,
    ua: req.body.requirementsUa,
  });
  await translationRequirements.save();

  const translationWillBePlus = new Internalization({
    key: keyWillBePlus,
    en: req.body.willBePlusEn,
    ru: req.body.willBePlusRu,
    ua: req.body.willBePlusUa,
  });
  await translationWillBePlus.save();

  const translationResponsibilities = new Internalization({
    key: keyResponsibilities,
    en: req.body.responsibilitiesEn,
    ru: req.body.responsibilitiesRu,
    ua: req.body.responsibilitiesUa,
  });
  await translationResponsibilities.save();

  const translationConditions = new Internalization({
    key: keyConditions,
    en: req.body.conditionsEn,
    ru: req.body.conditionsRu,
    ua: req.body.conditionsUa,
  });
  await translationConditions.save();

  const vacancy = new Vacancy({
    name: req.body.name,
    requirements: keyRequirements,
    willBePlus: keyWillBePlus,
    responsibilities: keyResponsibilities,
    conditions: keyConditions,
    isEnabled: req.body.isEnabled,
    specialization: req.body.specialization,
  });
  await vacancy.save();

  res.status(201).json(vacancy);
};

const getAllVacancies = async (req, res) => {
  const vacancies = await Vacancy.find();
  res.json(vacancies);
};

const updateVacancy = async (req, res) => {
  const vacancy = await Vacancy.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  if (!vacancy) throw HttpError[404]("Vacancy don't found!");

  const newReqTranslation = utils.generationObjTranslation(
    req.body.requirementsEn,
    req.body.requirementsRu,
    req.body.requirementsUa
  );
  const newWillBePlusTranslation = utils.generationObjTranslation(
    req.body.willBePlusEn,
    req.body.willBePlusRu,
    req.body.willBePlusUa
  );
  const newResponsibilitiesTranslation = utils.generationObjTranslation(
    req.body.responsibilitiesEn,
    req.body.responsibilitiesRu,
    req.body.responsibilitiesUa
  );
  const newConditionsTranslation = utils.generationObjTranslation(
    req.body.conditionsEn,
    req.body.conditionsRu,
    req.body.conditionsUa
  );

  const translationRequirements = await Internalization.findOneAndUpdate(
    { key: vacancy.requirements },
    { ...newReqTranslation }
  );
  if (!translationRequirements) utils.newTranslationData(Internalization, vacancy.requirements, newReqTranslation);

  const translationWillBePlus = await Internalization.findOneAndUpdate(
    { key: vacancy.willBePlus },
    { ...newWillBePlusTranslation }
  );
  if (!translationWillBePlus) utils.newTranslationData(Internalization, vacancy.willBePlus, newWillBePlusTranslation);

  const translationResponsibilities = await Internalization.findOneAndUpdate(
    { key: vacancy.responsibilities },
    { ...newResponsibilitiesTranslation }
  );
  if (!translationResponsibilities)
    utils.newTranslationData(Internalization, vacancy.responsibilities, newResponsibilitiesTranslation);

  const translationConditions = await Internalization.findOneAndUpdate(
    { key: vacancy.conditions },
    { ...newConditionsTranslation }
  );
  if (!translationConditions) utils.newTranslationData(Internalization, vacancy.conditions, newConditionsTranslation);

  res.json(vacancy);
};

const deleteVacancy = async (req, res) => {
  const vacancy = await Vacancy.findByIdAndDelete(req.params.id);
  if (!vacancy) throw HttpError[404]("Vacancy don't found!");

  await Internalization.findOneAndDelete({ key: vacancy.requirements });
  await Internalization.findOneAndDelete({ key: vacancy.willBePlus });
  await Internalization.findOneAndDelete({ key: vacancy.responsibilities });
  await Internalization.findOneAndDelete({ key: vacancy.conditions });

  res.json({ message: 'Success!' });
};

module.exports = {
  createVacancy,
  getAllVacancies,
  updateVacancy,
  deleteVacancy,
};
