const HttpError = require('http-errors');

const Technology = require('../models/Technology');
const Internalization = require('../models/Internalization');

const utils = require('../utils');

const createTechnology = async (req, res) => {
  if (!req.file) {
    throw HttpError[400]('Logo is required!');
  }

  const { body } = req;
  const keyTitle = `technology_title_${utils.makeRandomId()}`;
  const keySummary = `technology_summary_${utils.makeRandomId()}`;
  const keyDesc = `technology_desc_${utils.makeRandomId()}`;
  const logo = req.file.filename;

  const translationTitle = new Internalization({ key: keyTitle, en: body.titleEn, ru: body.titleRu, ua: body.titleUa });
  await translationTitle.save();

  const translationSummary = new Internalization({
    key: keySummary,
    en: body.summaryEn,
    ru: body.summaryRu,
    ua: body.summaryUa,
  });
  await translationSummary.save();

  const translationDesc = new Internalization({
    key: keyDesc,
    en: body.descEn,
    ru: body.descRu,
    ua: body.descUa,
  });
  await translationDesc.save();

  const technology = new Technology({
    title: keyTitle,
    summary: keySummary,
    description: keyDesc,
    logo,
  });
  await technology.save();

  res.status(201).json(technology);
};

const getAllTechnologies = async (req, res) => {
  const technology = await Technology.find();
  res.json(technology);
};

const updateTechnology = async (req, res) => {
  const technology = await Technology.findById(req.params.id);
  if (!technology) throw HttpError[404]("Technology don't found!");

  if (req.file) {
    utils.deleteFileIfExists(technology.logo, () => {
      technology.logo = req.file.filename;
    });
  }

  const newTitileTranslation = utils.generationObjTranslation(req.body.titleEn, req.body.titleRu, req.body.titleUa);
  const newSummaryTranslation = utils.generationObjTranslation(
    req.body.summaryEn,
    req.body.summaryRu,
    req.body.summaryUa
  );
  const newDescTranslation = utils.generationObjTranslation(req.body.descEn, req.body.descRu, req.body.descUa);

  const translationTitle = await Internalization.findOneAndUpdate(
    { key: technology.title },
    { ...newTitileTranslation }
  );
  if (!translationTitle) utils.newTranslationData(Internalization, technology.title, newTitileTranslation);

  const translationSummary = await Internalization.findOneAndUpdate(
    { key: technology.summary },
    { ...newSummaryTranslation }
  );
  if (!translationSummary) utils.newTranslationData(Internalization, technology.summary, newSummaryTranslation);

  const translationDesc = await Internalization.findOneAndUpdate(
    { key: technology.description },
    { ...newDescTranslation }
  );
  if (!translationDesc) utils.newTranslationData(Internalization, technology.description, newDescTranslation);

  await technology.update(technology);

  res.json(technology);
};

const deleteTechnology = async (req, res) => {
  const technology = await Technology.findByIdAndDelete(req.params.id);
  if (!technology) throw HttpError[404]("Technology don't found!");

  await Internalization.findOneAndDelete({ key: technology.title });
  await Internalization.findOneAndDelete({ key: technology.summary });
  await Internalization.findOneAndDelete({ key: technology.description });

  utils.deleteFileIfExists(technology.logo);

  res.json({ message: 'Success!' });
};

module.exports = {
  createTechnology,
  getAllTechnologies,
  updateTechnology,
  deleteTechnology,
};
