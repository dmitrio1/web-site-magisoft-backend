const HttpError = require('http-errors');
const Portfolio = require('../models/Portfolio');
const Internalization = require('../models/Internalization');

const utils = require('../utils');

const createProduct = async (req, res) => {
  if (!req.files || !req.files.logo || !req.files.slide) {
    throw HttpError[400]('All filds required');
  }

  const { descEn, descRu, descUa, title } = req.body;
  const keyDesc = `portfolio_desc_${utils.makeRandomId()}`;
  const logo = req.files.logo[0].filename;
  const slide = req.files.slide[0].filename;

  const translation = new Internalization({ key: keyDesc, en: descEn, ru: descRu, ua: descUa });
  await translation.save();

  const product = new Portfolio({ title, description: keyDesc, logo, slide });
  await product.save();

  res.status(201).json(product);
};

const getPortfolio = async (req, res) => {
  const products = await Portfolio.find();
  res.json(products);
};

const updateProduct = async (req, res) => {
  const product = await Portfolio.findById(req.params.id);
  if (!product) throw HttpError[404]("Product don't found!");

  if (req.files) {
    if (req.files.logo) {
      utils.deleteFileIfExists(product.logo, () => {
        product.logo = req.files.logo[0].filename;
      });
    }

    if (req.files.slide) {
      utils.deleteFileIfExists(product.slide, () => {
        product.slide = req.files.slide[0].filename;
      });
    }
  }

  if (req.body.title) product.title = req.body.title;

  const newDescTranslation = utils.generationObjTranslation(req.body.descEn, req.body.descRu, req.body.descUa);
  const translation = await Internalization.findOneAndUpdate({ key: product.description }, { ...newDescTranslation });
  if (!translation) utils.newTranslationData(Internalization, product.description, newDescTranslation);

  await product.update(product);

  res.json(product);
};

const deleteProduct = async (req, res) => {
  const product = await Portfolio.findByIdAndDelete(req.params.id);
  if (!product) throw HttpError[404]("Product don't found!");

  await Internalization.findOneAndDelete({ key: product.description });

  utils.deleteFileIfExists(product.logo);
  utils.deleteFileIfExists(product.slide);

  res.json({ message: 'Success!' });
};

module.exports = {
  createProduct,
  getPortfolio,
  updateProduct,
  deleteProduct,
};
