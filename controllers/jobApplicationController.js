const HttpError = require('http-errors');
const JobApplication = require('../models/JobApplication');
const utils = require('../utils');

const createApplication = async (req, res) => {
  if (!req.file) {
    throw HttpError[400]('File is required!');
  }

  const application = new JobApplication({
    name: req.body.name,
    email: req.body.email,
    message: req.body.message,
    cv: req.file.filename,
    vacancy: req.body.vacancyId,
  });

  await application.save();

  res.status(201).json({ message: 'Success!' });
};

const getAllApplications = async (req, res) => {
  const applications = await JobApplication.find().populate('vacancy', 'name specialization');

  res.json(applications);
};

const deleteJobApplications = async (req, res) => {
  const application = await JobApplication.findByIdAndDelete(req.params.id);
  if (!application) throw HttpError[404]("Job Application don't found!");

  utils.deleteFileIfExists(application.cv);

  res.json({ message: 'Success!' });
};

module.exports = {
  createApplication,
  getAllApplications,
  deleteJobApplications,
};
