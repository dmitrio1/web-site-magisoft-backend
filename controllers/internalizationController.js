const HttpError = require('http-errors');

const Internalization = require('../models/Internalization');
const utils = require('../utils');

const createTranslation = async (req, res) => {
  await utils.checkIfExist({ field: 'key', value: req.body.key }, Internalization, { entity: 'Translation' });

  const translation = new Internalization(req.body);

  await translation.save();

  res.status(201).json(translation);
};

const getAllTranslations = async (req, res) => {
  const translations = await Internalization.find();
  const transformTranslations = utils.transformTranslationData(translations);

  res.json(transformTranslations);
};

const updateTranslation = async (req, res) => {
  // const newTranslation = utils.generationObjTranslation(req.body.en, req.body.ru, req.body.ua);
  const translation = await Internalization.findOneAndUpdate({ key: req.body.key }, req.body, {
    new: true,
  });

  if (!translation) throw HttpError[404]("Translation don't found!");

  res.json(translation);
};

const deleteTranslation = async (req, res) => {
  if (!req.params.key) throw HttpError[404]("Translation don't found!");

  const translation = await Internalization.findOneAndDelete({ key: req.params.key });

  if (!translation) throw HttpError[404]("Translation don't found!");

  res.json({ message: 'Success!' });
};

module.exports = {
  createTranslation,
  getAllTranslations,
  updateTranslation,
  deleteTranslation,
};
