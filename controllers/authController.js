const HttpError = require('http-errors');
const passport = require('passport');

const utils = require('../utils');

const User = require('../models/User');

const register = async (req, res) => {
  const { email, password } = req.body;

  await utils.checkIfExist({ field: 'email', value: email }, User, { entity: 'Email' });

  const user = new User({ email, password });
  user.hashPassword(password);
  await user.save();

  res.status(201).json({ message: 'Success!' });
};

const login = (req, res, next) =>
  passport.authenticate('local', { session: false }, (err, passportUser) => {
    if (err) {
      return next(err);
    }

    if (!passportUser) {
      throw new HttpError[401]('Email or password are wrong');
    }
    const authResponse = passportUser.toAuthJSON();
    return res.json(authResponse);
  })(req, res, next);

module.exports = {
  register,
  login,
};
